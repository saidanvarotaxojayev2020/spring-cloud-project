package com.example.testgradlespringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestGradleSpringBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestGradleSpringBootApplication.class, args);
    }

}
