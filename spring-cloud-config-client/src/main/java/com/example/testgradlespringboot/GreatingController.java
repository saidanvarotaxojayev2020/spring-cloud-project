package com.example.testgradlespringboot;
/*
 * created by Saidanvar 27.01.2022
 */

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RefreshScope
@RestController
@RequestMapping("/api")
public class GreatingController {

    @Value("${gai.name:default value}")
    private String name;

    @GetMapping("/name")
    public String getGreating(){
        return name;
    }

}
